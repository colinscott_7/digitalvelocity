import { useState } from "react";
import * as rs from "reactstrap";
import { Icon } from "../../components/icon";

export const HomepageContentRange = (data: any) => {
  const [selectedProductsPage, setSelectedProductsPage] = useState(0);
  const chunkArray = (array: any, size: number) => {
    let result = [];
    for (let i = 0; i < array.length; i += size) {
      let chunk = array.slice(i, i + size);
      result.push(chunk);
    }
    return result;
  };
  const productsList = chunkArray(data.props, 2);

  const rangeWidget = () => {
    return (
      <rs.Row>
        {productsList[selectedProductsPage]?.map((item: any, index: number) => {
          return (
            <rs.Col sm={6} className="text-white text-center" key={`rangeItem_${index}`}>
              <div className="d-flex flex-column align-items-center">
                <img src={item.img} width={300} className="img-fluid" />
                <div className="py-2">
                  <Icon type="Solid" icon="star" className="text-secondary" />
                  <Icon type="Solid" icon="star" className="text-secondary" />
                  <Icon type="Solid" icon="star" className="text-secondary" />
                  <Icon type="Solid" icon="star" className="text-secondary" />
                  <Icon type="Solid" icon="star" />
                  <h2 className="h3 text-uppercase">{item.name}</h2>
                  <h3 className="h6 text-uppercase">{item.description}</h3>
                  <span className="mb-2 d-block">£{item.price}</span>
                </div>
              </div>
            </rs.Col>
          );
        })}
        <rs.Col sm={12} className="text-center">
          <rs.Button color="link text-white" onClick={() => setSelectedProductsPage(0)}>
            <Icon type={selectedProductsPage === 0 ? "Solid" : "Regular"} icon="circle" />
          </rs.Button>
          <rs.Button color="link text-white" onClick={() => setSelectedProductsPage(1)}>
            <Icon type={selectedProductsPage === 1 ? "Solid" : "Regular"} icon="circle" />
          </rs.Button>
          <rs.Button color="link text-white" onClick={() => setSelectedProductsPage(2)}>
            <Icon type={selectedProductsPage === 2 ? "Solid" : "Regular"} icon="circle" />
          </rs.Button>
        </rs.Col>
      </rs.Row>
    );
  };

  return (
    <div className="">
      <rs.Container>
        <rs.Row>
          <rs.Col
            lg={7}
            xl={6}
            className="ms-auto ps-3 pt-3 pe-3 p-lg-5 d-flex flex-column align-items-center align-items-lg-end justify-content-start justify-content-lg-center text-center text-lg-start"
          >
            <div className="homepage-feature-whey pt-5 pb-3 py-lg-5">
              <h3 className="h5 text-uppercase text-white">Why not try something new?!</h3>
              <h2 className="text-uppercase text-white">
                Get your hands on our latest and greatest
              </h2>
              <rs.Button color="light" className="text-uppercase h6 ms-auto me-0">
                View the range
              </rs.Button>
            </div>
          </rs.Col>
          <rs.Col lg={5} xl={6} className="me-auto d-flex align-items-center py-5">
            {rangeWidget()}
          </rs.Col>
        </rs.Row>
      </rs.Container>
    </div>
  );
};
