import * as rs from "reactstrap";
import { typeProduct } from "../../interfaces";

export const HomepageContentWhey = (data: typeProduct) => (
  <div className="bg-gradient-white-bottom animate__animated  animate__fadeIn">
    <div className="bg-layer" style={{ backgroundImage: "url('./whey-bg.png')" }}></div>
    <rs.Container>
      <rs.Row>
        <rs.Col
          lg={7}
          xl={6}
          className="ms-auto ps-3 pt-3 pe-3 p-lg-5 d-flex flex-column align-items-center align-items-lg-end justify-content-start justify-content-lg-center text-center text-lg-start"
        >
          <div className="homepage-feature-whey pt-5 pb-3 py-lg-5">
            <h2 className="text-uppercase text-white">
              Welcome back, Tom
              <br />
              looking for <span className="text-secondary">the usual?</span>
            </h2>
            <rs.Button color="dark" className="text-uppercase h6 ms-auto me-0">
              Shop now
            </rs.Button>
          </div>
        </rs.Col>
        <rs.Col lg={5} xl={6} className="me-auto overflow-hidden">
          <img
            src={data.img}
            width={500}
            className="img-fluid mx-auto d-block img-homepage-whey animate__animated  animate__bounce"
          />
        </rs.Col>
      </rs.Row>
    </rs.Container>
  </div>
);
