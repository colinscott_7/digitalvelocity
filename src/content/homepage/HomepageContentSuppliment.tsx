import * as rs from "reactstrap";
import { typeProduct } from "../../interfaces";

export const HomepageContentSuppliment = (data: typeProduct) => (
  <div className="bg-gradient-red-to-blue animate__animated  animate__fadeIn">
    <rs.Container>
      <rs.Row>
        <rs.Col lg={5} xl={6} className="ms-auto order-1 order-lg-0">
          <img
            src={data.img}
            className="img-fluid mx-auto d-block img-homepage-suppliment"
            width={500}
          />
        </rs.Col>
        <rs.Col
          lg={7}
          xl={6}
          className="me-auto order-0 order-lg-1 pt-5 ps-3 pe-3 p-lg-5 d-flex flex-column align-items-center align-items-lg-end justify-content-start justify-content-lg-center text-center text-lg-end"
        >
          <div className="py-3 py-lg-5">
            <h2 className="text-uppercase text-white">Looking for a quick boost?!</h2>
            <h3 className="h5 text-uppercase text-white">Everything you need in one place</h3>
            <rs.Button color="light" className="text-uppercase h6 mx-auto me-lg-0 ">
              The Suppliment
            </rs.Button>
          </div>
        </rs.Col>
      </rs.Row>
    </rs.Container>
  </div>
);
