import { useState } from "react";
import * as rs from "reactstrap";
import { Icon } from "../../components/icon";
import { typeProduct } from "../../interfaces";

export const HomepageContentSerum = (data: typeProduct) => {
  const [selectedProduct, setSelectedProduct] = useState(data.variations[0]);
  const serumProducts = data.variations;

  const productSelectorWidget = () => {
    return (
      <rs.Row>
        <rs.Col sm={5} className="d-flex align-items-center">
          <img src={selectedProduct.img} width={300} className="img-thumbnail img-fluid" />
        </rs.Col>
        <rs.Col sm={7}>
          <div className="py-5">
            <Icon type="Solid" icon="star" className="text-secondary" />
            <Icon type="Solid" icon="star" className="text-secondary" />
            <Icon type="Solid" icon="star" className="text-secondary" />
            <Icon type="Solid" icon="star" className="text-secondary" />
            <Icon type="Solid" icon="star" />
            <h2 className="h3 text-uppercase">The Serum</h2>
            <h3 className="h6 text-uppercase">{selectedProduct.name}</h3>
            <span className="mb-2 d-block">£{selectedProduct.price}</span>
            <div className="d-flex mb-3">
              {serumProducts?.map((item: any, index: number) => (
                <rs.Button
                  color="link"
                  className={`p-0 me-1 border-2 ${
                    selectedProduct.id === item.id ? "border-dark" : ""
                  }`}
                  onClick={() => setSelectedProduct(item)}
                  key={`serumBtn_${index}`}
                >
                  <img src={item.img} width={140} className="img-thumbnail img-fluid" />
                </rs.Button>
              ))}
            </div>
            <rs.Button color="dark" className="mb-2 text-uppercase h6" block>
              <Icon type="Solid" icon="shopping-cart" className="me-1" />
              add to basket
            </rs.Button>
            <rs.Button color="secondary" className="text-uppercase h6" block>
              <Icon type="Solid" icon="stopwatch" className="me-1" />
              quick buy
            </rs.Button>
          </div>
        </rs.Col>
      </rs.Row>
    );
  };
  return (
    <>
      <div className="position-relative bg-white">
        <div className="bg-layer-split">
          <div className="d-none d-lg-block bg-white bg-layer-split-content"></div>
          <div
            className="bg-layer-split-image"
            style={{ backgroundImage: "url('./serum-01.jpg')" }}
          ></div>
        </div>
        <rs.Container className="position-relative">
          <rs.Row>
            <rs.Col lg={8} xl={6} className="bg-white p-5 order-1 order-lg-0">
              {productSelectorWidget()}
            </rs.Col>
            <rs.Col
              lg={4}
              xl={6}
              className=" order-0 order-lg-1"
              style={{ minHeight: "400px" }}
            ></rs.Col>
          </rs.Row>
        </rs.Container>
      </div>
      <div className="position-relative bg-white">
        <div className="bg-layer-split">
          <div
            className="bg-layer-split-image"
            style={{ backgroundImage: "url('./serum-02.jpg')" }}
          ></div>
          <div className="d-none d-lg-block bg-white bg-layer-split-content"></div>
        </div>
        <rs.Container className="position-relative">
          <rs.Row>
            <rs.Col lg={4} xl={6} style={{ minHeight: "400px" }}></rs.Col>
            <rs.Col lg={8} xl={6} className="bg-white p-5">
              <div className="py-5 pe-5">
                <div className="h5">See what others are saying:</div>
                <blockquote>
                  <h2 className="h2 text-uppercase text-dark">
                    "A flavour that works for any mood"
                  </h2>
                </blockquote>
                <img src="./Trustpilot.png" className="d-block" />
                <rs.Button color="dark" className="text-uppercase h6">
                  find out more
                </rs.Button>
              </div>
            </rs.Col>
          </rs.Row>
        </rs.Container>
      </div>
    </>
  );
};
