import { typeDataPages } from "./interfaces/typeDataPages";

export const PagesData: typeDataPages = {
  data: [
    {
      id: 1,
      title: "The Whey",
      url: "/whey",
      hideNav: false,
    },
    {
      id: 2,
      title: "The Suppliment",
      url: "/suppliment",
      hideNav: false,
    },
    {
      id: 3,
      title: "The Boost",
      url: "/boost",
      hideNav: false,
    },
    {
      id: 4,
      title: "The Pre-Drink",
      url: "/pre-drink",
      hideNav: false,
    },
    {
      id: 5,
      title: "View all",
      url: "/all",
      hideNav: false,
    },
    {
      id: 6,
      title: "FAQs",
      url: "/faqs",
      hideNav: false,
    },
    {
      id: 7,
      title: "Contact",
      url: "/contact",
      hideNav: false,
    },
  ],
};
