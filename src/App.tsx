import { Loading } from "./components";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import "./scss/index.scss";

import { ViewHomepage } from "./views/ViewHomepage";
import React from "react";
import { typeDataProducts } from "./interfaces/typeDataProducts";
import axios from "axios";
import { ProductsData } from "./ProductsData";

export const App = () => {
  const [results, setResults] = React.useState<typeDataProducts>();

  React.useEffect(() => {
    // timeout for testing loading
    setTimeout(() => setResults(ProductsData), 500);
  }, []);

  if (!results) {
    return <Loading />;
  }

  return (
    <>
      <Router>
        <Switch>
          <Route path={"/"} exact component={() => <ViewHomepage data={results} />} />
        </Switch>
      </Router>
    </>
  );
};
