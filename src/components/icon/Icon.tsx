export interface typeIcon {
  type: string;
  icon: string;
  className?: string;
}

export const Icon = (props: typeIcon) => {
  const output = (param: string) => {
    switch (param) {
      case "Solid":
        return "fas";
      case "Regular":
        return "far";
      case "Brands":
        return "fab";
      default:
        return "fas";
    }
  };
  return (
    <>
      <i className={`${output(props.type)} fa-${props.icon} ${props.className}`}></i>
    </>
  );
};
