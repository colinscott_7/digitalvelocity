export const Loading = () => {
  return (
    <>
      <div className="loader">
        <div className="loading"></div>
        <div className="d-none">Loading...</div>
      </div>
    </>
  );
};
