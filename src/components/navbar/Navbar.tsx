import React from "react";
import { BrowserRouter as Router, Link, useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { typeNavbar, typePage } from "../../interfaces/";
import * as rs from "reactstrap";
import { Icon } from "../icon";

export const Navbar = (props: typeNavbar) => {
  const [menuVisible, setMenuVisible] = React.useState<boolean>(false);
  const [menuClasses, setMenuClasses] = React.useState<string>("");
  const history = useHistory();
  const location = useLocation();

  // Functions
  const handleMenuToggle = () => {
    if (menuVisible) {
      setMenuVisible(false);
      setMenuClasses("");
    }
    if (!menuVisible) {
      setMenuVisible(true);
      setMenuClasses("show");
    }
  };

  // Snippets
  const navList = props.data
    .filter((item: typePage) => item.hideNav !== true)
    .map((item: typePage, index: number) => {
      const navigateToPath = (url: string) => (evt: any) => {
        evt.preventDefault();
        evt.stopPropagation();
        history.push(url);
        setMenuVisible(false);
        setMenuClasses("");
      };
      return (
        <li className="nav-item" key={`navitem___${index}`}>
          <rs.Button color="link" onClick={navigateToPath(item.url)} className="nav-link">
            {item.title}
          </rs.Button>
        </li>
      );
    });
  const logo = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="157.494"
      height={40}
      className="navbar-brand-img"
      viewBox="0 0 157.494 40"
    >
      <path
        id="RePAIR"
        d="M154.255,40a1.469,1.469,0,0,1-1.551-1.183l-3.1-9.589c-.27-.774-.565-1.142-1.51-1.142h-.408a1.031,1.031,0,0,0-1.184,1.142v9.629A1.116,1.116,0,0,1,145.36,40h-2.244a1.063,1.063,0,0,1-1.1-1.142V12.052a1.21,1.21,0,0,1,1.142-1.183h5.956A8.433,8.433,0,0,1,155.31,13a8.432,8.432,0,0,1,2.127,6.2c0,3.271-1,5.647-2.978,7.059-.674.476-.863.755-.653,1.469l3.632,11.1a.933.933,0,0,1-.1.852.8.8,0,0,1-.672.33ZM146.5,16.214v6.528a1.185,1.185,0,0,0,1.184,1.183h.49c3.4,0,4.611-1.174,4.611-4.488,0-1.694-.314-2.751-1.02-3.427s-1.814-.979-3.591-.979h-.49A1.185,1.185,0,0,0,146.5,16.214ZM121.309,40a1.16,1.16,0,0,1-1.143-1.1V12.011a1.116,1.116,0,0,1,1.143-1.143h2.244a1.191,1.191,0,0,1,1.142,1.1V38.9a1.229,1.229,0,0,1-1.183,1.1Zm-19.915,0a1.308,1.308,0,0,1-1.387-1.183l-.815-3.958a1.444,1.444,0,0,0-1.428-1.224H93.887a1.556,1.556,0,0,0-1.346,1.224l-.775,4A1.39,1.39,0,0,1,90.379,40H88.257a.888.888,0,0,1-.715-.31,1.044,1.044,0,0,1-.182-.872l5.181-26.846a1.216,1.216,0,0,1,1.346-1.1h3.876a1.323,1.323,0,0,1,1.428,1.183l5.386,26.765a1.016,1.016,0,0,1-.188.849,1.064,1.064,0,0,1-.833.334ZM95.6,17.764,93.684,28.454a.8.8,0,0,0,.125.7,1.12,1.12,0,0,0,.895.355h2.326a.869.869,0,0,0,.979-1.142L96.05,17.723a.3.3,0,0,0-.248-.239C95.694,17.483,95.62,17.589,95.6,17.764ZM59.364,40a1.145,1.145,0,0,1-1.1-1.1V12.052a1.21,1.21,0,0,1,1.143-1.183h6.323c2.933,0,4.764.562,6.12,1.877,1.587,1.587,2.326,3.7,2.326,6.65,0,6.095-2.6,8.813-8.446,8.813H63.893a1.11,1.11,0,0,0-1.142,1.224v9.425A1.219,1.219,0,0,1,61.527,40Zm3.387-23.786v6.691a1.03,1.03,0,0,0,1.142,1.142h.816c1.859,0,3.011-.3,3.733-.958s1.04-1.712,1.04-3.407c0-1.885-.3-2.983-1.01-3.672s-1.831-.979-3.764-.979h-.816A1.125,1.125,0,0,0,62.751,16.214ZM29.682,40a1.229,1.229,0,0,1-1.1-1.183v-2.2a1.161,1.161,0,0,1,1.1-1.143H41.494a1.116,1.116,0,0,1,1.143,1.143v2.244A1.191,1.191,0,0,1,41.534,40ZM12.24,40a1.469,1.469,0,0,1-1.551-1.183l-3.1-9.589c-.271-.774-.565-1.142-1.509-1.142H5.671a1.031,1.031,0,0,0-1.183,1.142v9.629A1.117,1.117,0,0,1,3.345,40H1.1A1.063,1.063,0,0,1,0,38.858V12.052a1.21,1.21,0,0,1,1.143-1.183H7.1A8.432,8.432,0,0,1,13.3,13a8.432,8.432,0,0,1,2.127,6.2c0,3.271-1,5.647-2.978,7.059-.674.476-.863.754-.653,1.469l3.632,11.1a.935.935,0,0,1-.1.852.8.8,0,0,1-.672.33ZM4.488,16.214v6.528a1.185,1.185,0,0,0,1.183,1.183h.49c3.4,0,4.611-1.174,4.611-4.488,0-1.695-.314-2.751-1.02-3.427s-1.814-.979-3.591-.979h-.49A1.185,1.185,0,0,0,4.488,16.214ZM30.478,29.147c-1.007-1.248-1.346-2.587-1.346-5.3V18.009c0-3.062.4-4.358,1.714-5.549a7.47,7.47,0,0,1,4.978-1.591,6.887,6.887,0,0,1,5.1,1.8C42.2,13.987,42.6,15.321,42.6,18.376v2.081A1.071,1.071,0,0,1,41.453,21.6h-7.1a1.11,1.11,0,0,0-1.1,1.06v.775c0,2.826.707,3.877,2.611,3.877,2,0,2.5-.75,2.652-1.959.13-.866.355-1.305,1.224-1.305h1.754c.726,0,1.143.461,1.143,1.264a5.676,5.676,0,0,1-1.51,4.162,7.2,7.2,0,0,1-5.181,1.714C33.42,31.188,31.53,30.482,30.478,29.147Zm2.733-12.281c0,.734.353,1.061,1.143,1.061h2.938c.8,0,1.184-.347,1.184-1.061a2.314,2.314,0,0,0-2.612-2.488C33.556,14.378,33.211,15.937,33.211,16.867ZM1.1,4.529A1.229,1.229,0,0,1,0,3.346v-2.2A1.16,1.16,0,0,1,1.1,0h155.25a1.116,1.116,0,0,1,1.142,1.143V3.387a1.19,1.19,0,0,1-1.1,1.142Z"
        fill="#fff"
      />
    </svg>
  );
  const navbarActions = (
    <>
      <rs.Button
        color="link"
        className="me-2 navbar-icon-btn d-none d-lg-inline-block"
        type="button"
      >
        <Icon type="Regular" icon="comment-alt" />
      </rs.Button>
      <rs.Button
        color="link"
        className="me-2 navbar-icon-btn d-none d-lg-inline-block"
        type="button"
      >
        <Icon type="Regular" icon="user" />
      </rs.Button>
      <rs.Button color="link" className="navbar-icon-btn" type="button">
        <Icon type="Solid" icon="shopping-cart" />
      </rs.Button>
    </>
  );

  return (
    <rs.Navbar color={props.theme} expand="lg" className="header-navbar">
      <rs.Container>
        <rs.Row className="w-100 mb-lg-2">
          <rs.Col className="d-lg-none">
            <rs.Button
              className="navbar-toggler animate__animated  animate__fadeIn"
              type="button"
              aria-controls="navbarList"
              aria-expanded={menuVisible}
              aria-label="Toggle navigation"
              onClick={handleMenuToggle}
            >
              <Icon type="Solid" icon="bars" />
            </rs.Button>
          </rs.Col>
          <rs.Col>
            <Link className="navbar-brand animate__animated  animate__fadeIn" to="/">
              {logo}
            </Link>
          </rs.Col>
          <rs.Col className="text-end animate__animated  animate__fadeIn">{navbarActions}</rs.Col>
        </rs.Row>
        <rs.Row className="w-100 mb-2">
          <rs.Col>
            <div
              className={`collapse navbar-collapse animate__animated  animate__fadeIn ${menuClasses}`}
              id="navbarList"
            >
              <ul className="navbar-nav w-100 justify-content-between mb-2 mb-lg-0">{navList}</ul>
            </div>
          </rs.Col>
        </rs.Row>
        <rs.Row className={`w-100 d-none d-lg-flex text-white animate__animated  animate__fadeIn`}>
          <rs.Col className="text-center text-uppercase">
            <small className="navbar-moredetails">
              <Icon type="Solid" icon="truck" className="me-2" />
              Free next day delivery
            </small>
          </rs.Col>
          <rs.Col className="text-center text-uppercase">
            <small className="navbar-moredetails">
              <Icon type="Solid" icon="map-marker-alt" className="me-2" />
              Manufactured in the UK
            </small>
          </rs.Col>
          <rs.Col className="text-center text-uppercase">
            <small className="navbar-moredetails">
              <Icon type="Solid" icon="microscope" className="me-2" />
              CPD Compliant
            </small>
          </rs.Col>
        </rs.Row>
      </rs.Container>
    </rs.Navbar>
  );
};
