import React from "react";
import { useHistory } from "react-router-dom";
import { typeHomepage } from "../interfaces/typeHomepage";
import { Loading } from "../components";
import { Navbar } from "../components/navbar";
import { PagesData } from "../PagesData";
import {
  HomepageContentWhey,
  HomepageContentSuppliment,
  HomepageContentSerum,
  HomepageContentRange,
} from "../content/homepage/";
import { Footer } from "../components/footer";

export const ViewHomepage = (props: typeHomepage) => {
  const history = useHistory();

  if (!props.data) {
    return <Loading />;
  }

  const products = props.data.data;
  const productWhey = products[0];
  const productSuppliment = products[1];
  const productSerum = products[2];

  // OUTPUT
  return (
    <>
      <div className="wrapper bg-gradient-blue-to-dark">
        <div className="header">
          <Navbar data={PagesData.data} theme="transparent" />
        </div>
        <div className="content">
          <HomepageContentWhey {...productWhey} />
          <HomepageContentSuppliment {...productSuppliment} />
          <HomepageContentSerum {...productSerum} />
          <HomepageContentRange props={products} />
        </div>
        <Footer />
      </div>
    </>
  );
};
