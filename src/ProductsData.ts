import { HomepageContentSerum } from "./content/homepage/HomepageContentSerum";
import { HomepageContentSuppliment } from "./content/homepage/HomepageContentSuppliment";
import { HomepageContentWhey } from "./content/homepage/HomepageContentWhey";
import { typeDataProducts } from "./interfaces/typeDataProducts";

export const ProductsData: typeDataProducts = {
  data: [
    {
      id: 1,
      name: "The Whey",
      img: "./Products/TheWhey/RePAIR_Whey.png",
      price: "£9.99",
      description: "Available in 5 flavours",
      variations: [
        {
          id: 1.0,
          name: "The Whey",
          img: "./Products/TheWhey/RePAIR_Whey.png",
          price: "£9.99",
          variations: [],
        },
        {
          id: 1.1,
          name: "The Whey (Brown)",
          img: "./Products/TheWhey/RePAIR_Whey_Brown.png",
          price: "£9.99",
          variations: [],
        },
        {
          id: 1.2,
          name: "The Whey (Lime)",
          img: "./Products/TheWhey/RePAIR_Whey_Lime.png",
          price: "£9.99",
          variations: [],
        },
        {
          id: 1.3,
          name: "The Whey (Green)",
          img: "./Products/TheWhey/RePAIR_Whey_Green.png",
          price: "£9.99",
          variations: [],
        },
        {
          id: 1.4,
          name: "The Whey (Orange)",
          img: "./Products/TheWhey/RePAIR_Whey_Orange.png",
          price: "£9.99",
          variations: [],
        },
      ],
    },
    {
      id: 2,
      name: "The Suppliment",
      img: "./Products/TheSuppliment/RePAIR_Suppliments_Angle.png",
      price: "£9.99",
      description: "Available in 1 flavour",
      variations: [],
    },
    {
      id: 3,
      name: "The Serum",
      img: "./Products/TheSerum/RePAIR_Serum.png",
      price: "9.99",
      description: "Available in 5 flavours",
      variations: [
        {
          id: 3.0,
          name: "Fresh Lemon",
          img: "./Products/TheSerum/RePAIR_Serum.png",
          price: "9.99",
          variations: [],
        },
        {
          id: 3.1,
          name: "The Serum (Lime)",
          img: "./Products/TheSerum/RePAIR_Serum_Lime.png",
          price: "10.99",
          variations: [],
        },
        {
          id: 3.2,
          name: "The Serum (Pink)",
          img: "./Products/TheSerum/RePAIR_Serum_Pink.png",
          price: "11.99",
          variations: [],
        },
        {
          id: 3.3,
          name: "The Serum (Yellow)",
          img: "./Products/TheSerum/RePAIR_Serum_Yellow.png",
          price: "12.99",
          variations: [],
        },
        {
          id: 3.4,
          name: "The Serum (Violet)",
          img: "./Products/TheSerum/RePAIR_Serum_Violet.png",
          price: "13.99",
          variations: [],
        },
      ],
    },
    {
      id: 4,
      name: "The Gel",
      img: "./Products/TheGel/RePAIR_Gel.png",
      price: "4.99",
      description: "Available in 1 flavour",
      variations: [],
    },
    {
      id: 5,
      name: "The Oil",
      img: "./Products/TheOil/RePAIR_Oil.png",
      price: "7.99",
      description: "Available in 1 flavour",
      variations: [],
    },
    {
      id: 6,
      name: "The Oil Spray",
      img: "./Products/TheOilSpray/RePAIR_Spray.png",
      price: "13.99",
      description: "Available in 1 flavour",
      variations: [],
    },
  ],
};
