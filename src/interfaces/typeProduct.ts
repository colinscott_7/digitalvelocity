export interface typeProduct {
  id: number;
  name: string;
  img?: string;
  homepageContent?: any;
  variations: typeProduct[];
  price?: string;
  description?: string;
}
