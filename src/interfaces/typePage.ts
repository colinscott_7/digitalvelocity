export interface typePage {
  id: number;
  title: string;
  url: string;
  hideNav: boolean;
}
