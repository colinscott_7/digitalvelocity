export * from "./typeDataPages";
export * from "./typeDataProducts";
export * from "./typeHomepage";
export * from "./typeNavbar";
export * from "./typePage";
export * from "./typeProduct";
