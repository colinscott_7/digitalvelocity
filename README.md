# Digital Velocity - RePAIR

## Installation

- Please clone the repo and open in an IDE, works best with Visual Studio Code.
- Open a terminal and run `npm i` to install the dependencies.
- Then run `npm start` to load the project.
- The project will then be available at [localhost:3000](http://localhost:3000/)

## Technologies & Languages

- JS, React, Typescript, React Router, Reactstrap, SCSS, Bootstrap, Google Fonts, Fontawesome
